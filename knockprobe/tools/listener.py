#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

===============================================================================

Copyright (C) 2013/2016 Laurent Labatut / Laurent Champagnac



 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 ===============================================================================
"""
import json
import logging
import numbers
import os
import signal
import socket

import sys

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('listener')


def clean_socket():
    """

    :return:
    :rtype:
    """
    if os.path.exists(UDP_SOCKET_FILE):
        os.remove(UDP_SOCKET_FILE)


def receive_signal(signal, frame):
    """

    :param signal:
    :type signal:
    :param frame:
    :type frame:
    :return:
    :rtype:
    """
    logger.info('Process terminated  by signal %s at line %s', SIGNALS_TO_NAMES_DICT[signal] , frame.f_lineno)
    clean_socket()
    exit(0)


SIGNALS_TO_NAMES_DICT = dict((getattr(signal, n), n) for n in dir(signal) if n.startswith('SIG') and '_' not in n)
signal.signal(signal.SIGINT, receive_signal)
signal.signal(signal.SIGTERM, receive_signal)

UDP_SOCKET_FILE = '/tmp/mock.knockdaemon.socket'

if __name__ == '__main__':
    clean_socket()
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)  # UDP
    logger.info("this software is for mock use only.")
    logger.info("Ready to receive datagram on %s", UDP_SOCKET_FILE)

    try:
        sock.bind(UDP_SOCKET_FILE)

    except Exception as e:
        logger.warning(e)
        logger.warning('Exiting...')
        exit(127)

    item_recv_count = 0
    data_recv_count = 0

    while True:
        try:
            # receive data in blocking mode
            data, addr = sock.recvfrom(61440)  # buffer size is 61440 bytes

            data_recv_count += 1
            logger.info("%s: received message:%s", data_recv_count, data)

            try:
                json_data = json.loads(data)
            except Exception as e:
                logger.warning('Not json data %s', e)
                continue

            if not isinstance(json_data, list):
                logger.warning('Data must be list of tuple ("item", "type", float)')
                continue
            for item in json_data:
                if not isinstance(item[0], basestring):
                    logger.warning('item_name must be str')
                    continue
                if not isinstance(item[1], basestring):
                    logger.warning('item_type must be a str')
                if not isinstance(item[2], numbers.Real):
                    logger.warning('value must be a float')
                item_recv_count += 1
                logger.info('%s: recv: item_name=%s, item_type=%s, value=%s', item_recv_count, *item)
        except Exception as e:
            logger.warning("Error: %s", e)
            clean_socket()
            sys.exit(127)
