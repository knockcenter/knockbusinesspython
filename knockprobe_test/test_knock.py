"""
-*- coding: utf-8 -*-
===============================================================================

Copyright (C) 2013/2016 Laurent Labatut / Laurent Champagnac



 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 ===============================================================================
"""
import logging
import sys
from time import sleep, time
from unittest import TestCase

import knockprobe
from knockprobe import Knock
from knockprobe_test.ForTest.UdpMock import UdpMock

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
logger.info('Python version %s', sys.version_info)


class TestKnock(TestCase):
    """
    Tests
    """

    def setUp(self):
        """
        Setup
        """

        # Force for test
        Knock.UDP_SOCKET_NAME = Knock.UDP_UNITTEST_SOCKET_NAME

        # Start UDP mock
        self.um = UdpMock()
        self.um.start()

    def tearDown(self):
        """
        Tear down
        """

        if self.um:
            self.um.stop()
            self.um = None

    def _wait_for_recv(self, target_count=1, timeout_ms=2000):
        """
        Wait for mock recv
        :param target_count: int
        :type target_count: int
        :param timeout_ms: int
        :type timeout_ms: int
        """

        # Wait for recv
        ms = int(round(time() * 1000))
        while True:
            if len(self.um.recv_list) >= target_count:
                break

            ms_elapsed = int(round(time() * 1000)) - ms
            if ms_elapsed > timeout_ms:
                break
            else:
                sleep(0.1)

        self.assertGreaterEqual(len(self.um.recv_list), target_count)

    def test_increment(self):
        """
        Test
        """
        logger.info('start test_increment')
        Knock.increment('toto', 1)
        Knock.increment('toto', 2)
        Knock.increment('toto', 3)
        Knock.increment('toto', 4)
        Knock.increment('toto', -5)

        # Check queues
        self.assertEqual(Knock.QUEUE_COUNTER.qsize(), 5)
        ar = list(Knock.QUEUE_COUNTER.queue)
        for i in range(0, 4):
            self.assertEqual(ar[i][0], "toto")
            self.assertEqual(ar[i][1], float(i + 1))
        for i in range(4, 5):
            self.assertEqual(ar[i][0], "toto")
            self.assertEqual(ar[i][1], -float(i + 1))
        self.assertEqual(Knock.QUEUE_DTC.qsize(), 0)
        self.assertEqual(Knock.QUEUE_GAUGE.qsize(), 0)

        # Commit
        Knock.commit()

        # Check
        self.assertEqual(Knock.QUEUE_COUNTER.qsize(), 0)
        self.assertEqual(Knock.QUEUE_DTC.qsize(), 0)
        self.assertEqual(Knock.QUEUE_GAUGE.qsize(), 0)

        # Wait
        self._wait_for_recv(target_count=1)

        # Check
        self.assertGreater(len(self.um.recv_list), 0)
        self.assertEqual(self.um.recv_list[0][0], "toto")
        self.assertEqual(self.um.recv_list[0][1], "C")
        self.assertEqual(self.um.recv_list[0][2], 5.0)

    def test_gauge(self):
        """
        Test
        """
        logger.info('start test_gauge')
        Knock.gauge('toto', 1)
        Knock.gauge('toto', 2)
        Knock.gauge('toto', 3)
        Knock.gauge('toto', 4)
        Knock.gauge('toto', -5)

        # Check queues
        self.assertEqual(Knock.QUEUE_GAUGE.qsize(), 5)
        ar = list(Knock.QUEUE_GAUGE.queue)
        for i in range(0, 4):
            self.assertEqual(ar[i][0], "toto")
            self.assertEqual(ar[i][1], float(i + 1))
        for i in range(4, 5):
            self.assertEqual(ar[i][0], "toto")
            self.assertEqual(ar[i][1], -float(i + 1))
        self.assertEqual(Knock.QUEUE_DTC.qsize(), 0)
        self.assertEqual(Knock.QUEUE_COUNTER.qsize(), 0)

        # Commit
        Knock.commit()

        # Check
        self.assertEqual(Knock.QUEUE_COUNTER.qsize(), 0)
        self.assertEqual(Knock.QUEUE_DTC.qsize(), 0)
        self.assertEqual(Knock.QUEUE_GAUGE.qsize(), 0)

        # Wait
        self._wait_for_recv(target_count=1)

        # Check
        self.assertGreater(len(self.um.recv_list), 0)
        self.assertEqual(self.um.recv_list[0][0], "toto")
        self.assertEqual(self.um.recv_list[0][1], "G")
        self.assertEqual(self.um.recv_list[0][2], -5.0)

    def test_gauge_utf8(self):
        """
        Test
        """
        logger.info('start test_gauge')
        k_name = u"BUF\u001B\u0BD9\U0001A10D\u1501FUB"
        Knock.gauge(k_name, 1)
        Knock.gauge(k_name, 2)
        Knock.gauge(k_name, 3)
        Knock.gauge(k_name, 4)
        Knock.gauge(k_name, -5)

        # Check queues
        self.assertEqual(Knock.QUEUE_GAUGE.qsize(), 5)
        ar = list(Knock.QUEUE_GAUGE.queue)
        for i in range(0, 4):
            self.assertEqual(ar[i][0], k_name)
            self.assertEqual(ar[i][1], float(i + 1))
        for i in range(4, 5):
            self.assertEqual(ar[i][0], k_name)
            self.assertEqual(ar[i][1], -float(i + 1))
        self.assertEqual(Knock.QUEUE_DTC.qsize(), 0)
        self.assertEqual(Knock.QUEUE_COUNTER.qsize(), 0)

        # Commit
        Knock.commit()

        # Check
        self.assertEqual(Knock.QUEUE_COUNTER.qsize(), 0)
        self.assertEqual(Knock.QUEUE_DTC.qsize(), 0)
        self.assertEqual(Knock.QUEUE_GAUGE.qsize(), 0)

        # Wait
        self._wait_for_recv(target_count=1)

        # Check
        self.assertGreater(len(self.um.recv_list), 0)
        self.assertEqual(self.um.recv_list[0][0], k_name)
        self.assertEqual(self.um.recv_list[0][1], "G")
        self.assertEqual(self.um.recv_list[0][2], -5.0)

    def test_delay_to_count(self):
        """
        Test
        """
        logger.info('start test_delay_to_count')
        d1 = Knock.start_delay('time1')
        sleep(0.5)
        Knock.stop_delay(d1)

        d2 = Knock.start_delay('time2')
        sleep(1.0)
        Knock.stop_delay(d2)

        # Check queues
        self.assertEqual(Knock.QUEUE_DTC.qsize(), 2)
        ar = list(Knock.QUEUE_DTC.queue)
        self.assertEqual(ar[0][0], "time1")
        self.assertIsInstance(ar[0][1], float)
        self.assertGreaterEqual(ar[0][1], 500.0)
        self.assertLessEqual(ar[0][1], 600.0)
        self.assertEqual(ar[1][0], "time2")
        self.assertGreaterEqual(ar[1][1], 1000.0)
        self.assertLessEqual(ar[1][1], 1100.0)

        # Commit
        Knock.commit()

        # Check
        self.assertEqual(Knock.QUEUE_COUNTER.qsize(), 0)
        self.assertEqual(Knock.QUEUE_DTC.qsize(), 0)
        self.assertEqual(Knock.QUEUE_GAUGE.qsize(), 0)

        # Wait
        self._wait_for_recv(target_count=1)

        # Check
        self.assertGreater(len(self.um.recv_list), 0)
        self.assertEqual(self.um.recv_list[0][0], "time1")
        self.assertEqual(self.um.recv_list[0][1], "DTC")
        self.assertEqual(round(self.um.recv_list[0][2], 5), round(ar[0][1], 5))
        self.assertGreaterEqual(self.um.recv_list[0][2], 500.0)
        self.assertLessEqual(self.um.recv_list[0][2], 600.0)

        self.assertEqual(self.um.recv_list[1][0], "time2")
        self.assertEqual(self.um.recv_list[1][1], "DTC")
        self.assertEqual(round(self.um.recv_list[1][2], 5), round(ar[1][1], 5))
        self.assertGreaterEqual(self.um.recv_list[1][2], 1000.0)
        self.assertLessEqual(self.um.recv_list[1][2], 1100.0)

    def test_delay_to_count_with(self):
        """
        Test
        """
        logger.info('start test_delay_to_count_with')
        with Knock.with_delay('time1'):
            sleep(0.1)

        with Knock.with_delay('time2'):
            sleep(0.1)

        # Check queues
        self.assertEqual(Knock.QUEUE_DTC.qsize(), 2)
        ar = list(Knock.QUEUE_DTC.queue)
        self.assertEqual(ar[0][0], "time1")
        self.assertIsInstance(ar[0][1], float)
        self.assertEqual(ar[1][0], "time2")
        self.assertIsInstance(ar[1][1], float)

        # Commit
        Knock.commit()

        # Check
        self.assertEqual(Knock.QUEUE_COUNTER.qsize(), 0)
        self.assertEqual(Knock.QUEUE_DTC.qsize(), 0)
        self.assertEqual(Knock.QUEUE_GAUGE.qsize(), 0)

        # Wait
        self._wait_for_recv(target_count=1)

        # Check
        self.assertGreater(len(self.um.recv_list), 0)
        self.assertEqual(self.um.recv_list[0][0], "time1")
        self.assertEqual(self.um.recv_list[0][1], "DTC")
        self.assertEqual(round(self.um.recv_list[0][2], 5), round(ar[0][1], 5))

        self.assertEqual(self.um.recv_list[1][0], "time2")
        self.assertEqual(self.um.recv_list[1][1], "DTC")
        self.assertEqual(round(self.um.recv_list[1][2], 5), round(ar[1][1], 5))

    def test_delay_to_count_decorator(self):
        """
        Test
        """
        logger.info('start test_delay_to_count_decorator')

        @Knock.delay('time1')
        def timed_func1():
            sleep(0.1)

        @Knock.delay('time2')
        def timed_func2():
            sleep(0.1)

        timed_func1()
        timed_func2()

        # Check queues
        self.assertEqual(Knock.QUEUE_DTC.qsize(), 2)
        ar = list(Knock.QUEUE_DTC.queue)
        self.assertEqual(ar[0][0], "time1")
        self.assertIsInstance(ar[0][1], float)

        self.assertEqual(ar[1][0], "time2")
        self.assertIsInstance(ar[1][1], float)

        # Commit
        Knock.commit()

        # Check
        self.assertEqual(Knock.QUEUE_COUNTER.qsize(), 0)
        self.assertEqual(Knock.QUEUE_DTC.qsize(), 0)
        self.assertEqual(Knock.QUEUE_GAUGE.qsize(), 0)

        # Wait
        self._wait_for_recv(target_count=1)

        # Check
        self.assertGreater(len(self.um.recv_list), 0)
        self.assertEqual(self.um.recv_list[0][0], "time1")
        self.assertEqual(self.um.recv_list[0][1], "DTC")
        self.assertEqual(round(self.um.recv_list[0][2], 5), round(ar[0][1], 5))

        self.assertEqual(self.um.recv_list[1][0], "time2")
        self.assertEqual(self.um.recv_list[1][1], "DTC")
        self.assertEqual(round(self.um.recv_list[1][2], 5), round(ar[1][1], 5))

    def test_bench_all_no_mock(self):
        """
        Test
        """

        # Stop UDP for this one
        self.um.stop()
        self.um = None

        for item_count in [2, 100, 1000, 10000, 100000]:
            start_time = time()
            for cur_id in knockprobe.core.custom_range(item_count):
                Knock.gauge('counter', cur_id)
                Knock.increment('gauge', cur_id)
                d = Knock.start_delay('dtc')
                Knock.stop_delay(d)
            Knock.commit()
            elapsed_time = time() - start_time
            logger.info('items_counts=%s time=%s ps=%s', item_count, elapsed_time, 1.0 * item_count / elapsed_time)

    def test_bench_all_mock(self):
        """
        Test
        """

        for item_count in [2, 100, 1000, 4800, 500000]:
            self.um.recv_list = list()
            logger.info("Pushing")
            start_time = time()
            for cur_id in knockprobe.core.custom_range(item_count):
                # 1
                Knock.gauge('counter', cur_id)
                # 1
                Knock.increment('gauge', cur_id)
                # item_count
                d = Knock.start_delay('dtc')
                Knock.stop_delay(d)

            # Send
            logger.info("Sending")
            send_time = time()
            Knock.commit()
            elapsed_time = time() - start_time
            logger.info('items_counts=%s time=%s ps=%s', item_count, elapsed_time, 1.0 * item_count / elapsed_time)

            # Wait for recv
            logger.info("Waiting for recv")
            target_count = item_count + 2
            self._wait_for_recv(target_count=target_count, timeout_ms=30000)
            self.assertEqual(len(self.um.recv_list), target_count)
            elapsed_time = time() - send_time
            logger.info('recv_counts=%s time=%s ps=%s', len(self.um.recv_list), elapsed_time, 1.0 * len(self.um.recv_list) / elapsed_time)

            logger.info("Done")
