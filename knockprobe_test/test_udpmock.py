"""
-*- coding: utf-8 -*-
===============================================================================

Copyright (C) 2013/2016 Laurent Labatut / Laurent Champagnac



 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 ===============================================================================
"""
import logging
import sys
from unittest import TestCase

from knockprobe import Knock
from knockprobe_test.ForTest.UdpMock import UdpMock

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
logger.info('Python version %s', sys.version_info)


class TestUdpMock(TestCase):
    """
    Tests
    """

    def test_start_stop(self):
        """
        Test
        """

        # Force for test
        Knock.UDP_SOCKET_NAME = Knock.UDP_UNITTEST_SOCKET_NAME

        for _ in range(0, 10):
            um = UdpMock()

            # Start
            um.start()
            self.assertIsNotNone(um._soc)
            self.assertIsNotNone(um._thread)
            self.assertTrue(um._is_started)
            self.assertTrue(um._thread.isAlive())

            # Stop
            um.stop()
            self.assertIsNone(um._soc)
            self.assertIsNone(um._thread)
            self.assertFalse(um._is_started)
