"""
-*- coding: utf-8 -*-
===============================================================================

Copyright (C) 2013/2016 Laurent Labatut / Laurent Champagnac



 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 ===============================================================================
"""
import errno
import logging
import socket
import ujson

import sys

if sys.version_info > (3, 0):
    # noinspection PyPep8Naming,PyUnresolvedReferences
    import queue as Queue
else:
    import Queue
from threading import Thread
from time import time, sleep

import os

from knockprobe import Knock

logger = logging.getLogger(__name__)


class UdpMock(object):
    """
    Udp mock
    """

    def __init__(self, socket_name=None):
        """
        Init
        :param socket_name: str, None
        :type socket_name: str, None
        """

        if not socket_name:
            self._socket_name = Knock.UDP_SOCKET_NAME
        else:
            self._socket_name = socket_name
        self._soc = None
        self._is_started = False
        self._thread = None
        self._thread_proc = None

        # Udp input queue
        self._queue = Queue.Queue()

        # Stats & recv stuff
        self.recv_count = 0
        self.recv_ex = 0
        self.recv_list = list()

    def start(self):
        """
        Start
        """

        if self._is_started:
            logger.warn("Already started, bypass")
            return

        # Listen
        logger.info("Binding")

        # Alloc
        self._soc = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
        if os.path.exists(self._socket_name):
            os.remove(self._socket_name)

        # Switch to non blocking
        self._soc.setblocking(0)

        # Bind
        self._soc.bind(self._socket_name)

        # Recv buffer
        logger.info("Recv buf=%s", self._soc.getsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF))

        # Start recv thread (async)
        logger.info("Start thread")
        self._is_started = True
        self._thread = Thread(target=self._th_recv)
        self._thread.start()
        self._thread_proc = Thread(target=self._th_proc)
        self._thread_proc.start()

        # Wait for thread to run (5 sec for start allowed)
        ms = int(round(time() * 1000))
        while True:
            if self._thread.isAlive() and self._thread_proc.isAlive():
                break
            ms_elapsed = int(round(time() * 1000)) - ms
            if ms_elapsed > 5000:
                break
            else:
                sleep(0.1)
        if not self._thread.isAlive():
            raise Exception("Thread not alive")
        if not self._thread_proc.isAlive():
            raise Exception("Thread proc not alive")

        # Ok
        logger.info("Started")

    def _th_recv(self):
        """
        Recv thread
        """

        logger.info("Recv loop started")
        try:
            while self._is_started:
                try:
                    data = self._soc.recv(61440)
                    self._queue.put(data)
                except socket.error as e:
                    err = e.args[0]
                    if err == errno.EAGAIN or err == errno.EWOULDBLOCK:
                        # No data available, reloop
                        sleep(0.1)
                        continue
                    else:
                        # Socket error
                        logger.warn("Socket error, ex=%s", e)
                        self.recv_ex += 1
                except Exception as e:
                    logger.warn("Exception in loop, ex=%s", e)
                    self.recv_ex += 1
        except Exception as e:
            logger.warn("Exception out loop, ex=%s", e)
            self.recv_ex += 1
        finally:
            logger.info("Recv loop exited")

    def _th_proc(self):
        """
        Proc thread
        """

        logger.info("Proc loop started")
        try:
            while self._is_started:
                try:
                    data = self._queue.get_nowait()
                    self._process_data(data)
                except Queue.Empty:
                    sleep(0.0001)
                    continue
                except Exception as e:
                    logger.warn("Exception in loop, ex=%s", e)
                    self.recv_ex += 1
        except Exception as e:
            logger.warn("Exception out loop, ex=%s", e)
            self.recv_ex += 1
        finally:
            logger.info("Proc loop exited")

    def _process_data(self, data):
        """
        Process data
        :param data: binary buffer
        :type data: str
        """

        logger.debug("Processing, data=%s", repr(data))

        # Load json
        data_json = ujson.loads(data.strip())

        # Process
        for item, cur_type, value in data_json:
            logger.debug("Processing, item=%s, cur_type=%s, value=%s", repr(item), cur_type, value)
            self.recv_list.append((item, cur_type, value))
        logger.info("Recv ok, qsize=%s, data len=%s, cur_list=%s", self._queue.qsize(), len(data_json), len(self.recv_list))

    def stop(self):
        """
        Stop
        """

        if not self._is_started:
            logger.warn("Not started, bypass")
            return

        # Signal
        logger.info("Stopping")
        self._is_started = False

        # Wait to thread to complete
        logger.info("Waiting")
        self._thread.join()
        self._thread_proc.join()

        # Close socket
        logger.info("Closing")
        try:
            self._soc.shutdown(2)
        except Exception as e:
            logger.debug("Socket shutdown ex=%s", e)

        try:
            self._soc.close()
        except Exception as e:
            logger.debug("Socket close ex=%s", e)

        try:
            del self._soc
        except Exception as e:
            logger.debug("Socket del ex=%s", e)

        try:
            if os.path.exists(self._socket_name):
                os.remove(self._socket_name)
        except Exception as e:
            logger.debug("Socket file remove ex=%s", e)

        # Reset
        self._thread = None
        self._thread_proc = None
        self._soc = None
        self._queue = Queue.Queue()
        logger.info("Stopped")
